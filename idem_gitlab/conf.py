CONFIG = {}

CLI_CONFIG = {}

SUBCOMMANDS = {
    "gitlab": {
        "help": "Create idem_aws state modules by parsing the reset api",
        "dyne": "pop_create",
    },
}

DYNE = {"pop_create": ["autogen"], "states": ["states"], "exec": ["exec"]}
